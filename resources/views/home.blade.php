@extends('layout')

@section('title', 'Home')

@section('content')
    <h1>Pantalla de inicio</h1>
    <a class="button" href={{route('personas.index')}}>Personas</a>
@endsection
