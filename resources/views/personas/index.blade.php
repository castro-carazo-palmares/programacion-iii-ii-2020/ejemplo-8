@extends('layout')

@section('title', 'Personas')

@section('content')

    <h1>Personas</h1>

    <a class="button" href="{{ route('personas.create') }}">Insertar Persona</a>

    <ul>
        @forelse ($personas as $persona)
            <li><a class="link" href="{{ route('personas.show', $persona) }}">{{ $persona->nombre }}</a></li>
        @empty
            <li>No hay personas para mostrar</li>
        @endforelse

    </ul>
@endsection
