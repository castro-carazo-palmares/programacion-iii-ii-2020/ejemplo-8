@extends('layout')

@section('title', 'Insertar persona')

@section('content')
    <h1>Insertar nueva persona</h1>

    @if($errors->any())
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <div class="form">
        <form action="{{ route('personas.store') }}" method="POST">
            @csrf

            <label for="cedula">Cédula</label>
            <input type="number" name="cedula">

            <label for="nombre">Nombre</label>
            <input type="text" name="nombre">

            <label for="apellido">Apellido</label>
            <input type="text" name="apellido">

            <label for="ciudad">Ciudad</label>
            <input type="text" name="ciudad">

            <div class="actions">
                <button>Enviar</button>
                <a class="button" href={{ route('personas.index') }}>Volver</a>
            </div>
        </form>
    </div>
@endsection
