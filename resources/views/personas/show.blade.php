@extends('layout')

@section('title', 'Mostrar persona')

@section('content')

    <h1>{{ $persona->nombre }}</h1>

    <a class="button" href={{ route('personas.edit', $persona) }}>Editar</a>

    <form action="{{ route('personas.destroy', $persona) }}" method="POST">
        @csrf @method('DELETE')
        <button>Eliminar</button>
    </form>

    <p>Cédula: {{ $persona->cedula }}</p>
    <p>Nombre completo: {{ $persona->nombre . ' ' . $persona->apellido }}</p>
    <p>Ciudad: {{ $persona->ciudad }}</p>

    <a class="button" href={{ route('personas.index') }}>Volver</a>
@endsection
