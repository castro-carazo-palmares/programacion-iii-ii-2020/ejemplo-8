@extends('layout')

@section('title', 'Editar persona')

@section('content')
    <h1>Editar persona</h1>

    @if($errors->any())
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <div class="form">
        <form action="{{ route('personas.update', $persona) }}" method="POST">
            @csrf
            @method('PATCH')

            <label for="cedula">Cédula</label>
            <input type="number" name="cedula" value="{{ $persona->cedula }}" readonly>

            <label for="nombre">Nombre</label>
            <input type="text" name="nombre" value="{{ $persona->nombre }}">

            <label for="apellido">Apellido</label>
            <input type="text" name="apellido" value="{{ $persona->apellido }}">

            <label for="ciudad">Ciudad</label>
            <input type="text" name="ciudad" value="{{ $persona->ciudad }}">

            <div class="actions">
                <button>Enviar</button>
                <a class="button" href={{ route('personas.show', $persona) }}>Volver</a>
            </div>
        </form>
    </div>
@endsection
